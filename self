#!/bin/bash
# Title:          self - Simple Effective Lightweight Firewall
# Author:         Nathan Sorensen
# License:        GPLv3 - http://www.gnu.org/licenses/gpl-3.0.txt
#
# Description:    self is a iptables wrapper that allows you to quickly configure and make changes to services or ports that need to be opened for traffic.
#
# Usage:          [*] cp self/ /opt/
#                 [*] sudo chown -R root:root /opt/self/
#                 [*] sudo chmod 700 /opt/self/config
#                 [*] sudo vi|nano|pico /opt/self/config
#                      [+] make changes to the necessary lines to allow or disallow services.
#                 [*] sudo /opt/self/self start (starts self)
#                 [*] sudo /opt/self/self status (outputs iptables -L -vn)
case "$1" in
	start)
	# load configuration file
	self_config="/opt/self/config"
	if [ ! -r $self_config ]; then
		echo -e "Could not locate or open $self_config"
		exit 1
	fi
	#
	# flush and reset tables, chains, and policies
	iptables -P INPUT ACCEPT
	iptables -P OUTPUT ACCEPT
	iptables -P FORWARD ACCEPT
	iptables -F
	iptables -X
	iptables -Z
	#
	# default policies
	iptables -P INPUT DROP
	iptables -P OUTPUT ACCEPT
	iptables -P FORWARD DROP
	#
	# self_* chains
	iptables -N self_REJECT
	iptables -N self_DROP
	iptables -N self_SERVICES
	iptables -N self_SERVICES_SEC
	#
	# self_REJECT rules
	#iptables -A self_REJECT -p tcp -m limit --limit 7200/h -j LOG --log-prefix "TCP REJECT "
	iptables -A self_REJECT -p tcp -j REJECT --reject-with tcp-reset
	#iptables -A self_REJECT -p udp -m limit --limit 7200/h -j LOG --log-prefix "UDP REJECT "
	iptables -A self_REJECT -p udp -j REJECT --reject-with icmp-port-unreachable
	#iptables -A self_REJECT -p icmp -m limit --limit 7200/h -j LOG --log-prefix "ICMP REJECT "
	iptables -A self_REJECT -p icmp -j REJECT --reject-with icmp-host-unreachable
	#iptables -A self_REJECT -m limit --limit 7200/h -j LOG --log-prefix "VARIOUS REJECT "
	iptables -A self_REJECT -j REJECT --reject-with icmp-proto-unreachable
	iptables -A self_REJECT -p ALL -j RETURN
	#
	# self_DROP rules
	#iptables -A self_DROP -m limit --limit 7200/h -j LOG --log-prefix "SCAN DROP "
	iptables -A self_DROP -j DROP
	# self_DROP scans
	iptables -A INPUT -p tcp --tcp-flags ALL NONE -j self_DROP
	iptables -A INPUT -p tcp --tcp-flags ALL ALL -j self_DROP
	#
	# self_SERVICES_SEC rules
	iptables -A self_SERVICES_SEC -p tcp --syn -m limit --limit 2/s -j ACCEPT
	iptables -A self_SERVICES_SEC -p tcp ! --syn -m state --state NEW -j DROP
	iptables -A self_SERVICES_SEC -p ALL -j RETURN
	#
	# self_SERVICES rules
	. $self_config
	for p in $tcp_oPorts; do
		iptables -A self_SERVICES -p tcp --dport $p -m state --state NEW,ESTABLISHED,RELATED -j self_SERVICES_SEC
		iptables -A self_SERVICES -p tcp --dport $p -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
	done
	for p in $udp_oPorts; do
		iptables -A self_SERVICES -p udp --dport $p -m state --state NEW,ESTABLISHED,RELATED -j self_SERVICES_SEC
		iptables -A self_SERVICES -p udp --dport $p -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
	done
	for i in $trusted_IPs; do
		for p in $tcp_pPorts; do
			iptables -A self_SERVICES -p tcp -s $i --dport $p -m state --state NEW -j self_SERVICES_SEC
			iptables -A self_SERVICES -p tcp -s $i --dport $p -m state --state NEW -j ACCEPT
		done
	done
	#
	# block specific IPs
	for p in $blocked_IPs; do
		iptables -A INPUT -s $p -j DROP
	done
	#
	# INPUT rules
	iptables -A INPUT -i lo -j ACCEPT
	iptables -A INPUT -s 127.0.0.1 -j ACCEPT
	iptables -A INPUT -p ALL -m state --state ESTABLISHED,RELATED -j ACCEPT
	iptables -A INPUT -p ALL -j self_SERVICES
	iptables -A INPUT -p ALL -m limit --limit 10/s -j self_REJECT
	iptables -A INPUT -p ALL -j DROP
	#
	# OUTPUT rules
	iptables -A OUTPUT -o lo -j ACCEPT
	iptables -A OUTPUT -d 127.0.0.1 -j ACCEPT
	iptables -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
	iptables -A OUTPUT -p ALL -j ACCEPT
	;;

	stop)
	echo -e "self is stopping all packet filtering"
	# flush and reset tables, chains, and policies
	iptables -P INPUT ACCEPT
    iptables -P OUTPUT ACCEPT
    iptables -P FORWARD ACCEPT
    iptables -F
    iptables -X
	iptables -Z
	echo -e "self is stopping all IP routing"
	echo 0 > /proc/sys/net/ipv4/ip_forward
	;;

	status)
	echo -e "Status"
	iptables -L -vn
	;;

	*)
	echo -e "Unknown option: $0"
	echo -e "Valid options are: {start|stop|status}"
	exit 1
	;;

esac
